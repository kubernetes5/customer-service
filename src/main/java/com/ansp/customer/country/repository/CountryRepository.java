package com.ansp.customer.country.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ansp.customer.entities.*;

@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {

}
