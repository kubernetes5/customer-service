package com.ansp.customer.country.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.customer.country.service.CountryService;
import com.ansp.customer.entities.Country;

@RestController
public class CountryController {
	
	@Autowired
	private CountryService service;
	
	@GetMapping("/countries")
	public List<Country> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("/countries")
	public Country save(@RequestBody Country country) {
		return this.service.save(country);
	}
	
	@PutMapping("/countries/{id}")
	public Country update(@RequestBody Country country, @PathVariable("id") Integer id) {
		country.setId(id);
		return this.service.update(country);
	}
	
	@GetMapping("/countries/{id}")
	public Optional<Country> update(@PathVariable("id") Integer id) {
		return this.service.findById(id);
	}
	
	@DeleteMapping("/countries/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
