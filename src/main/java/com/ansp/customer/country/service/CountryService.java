package com.ansp.customer.country.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.customer.country.repository.CountryRepository;
import com.ansp.customer.entities.Country;

@Service
public class CountryService {

	@Autowired
	private CountryRepository repository;
	
	public List<Country> findAll(){
		return (List<Country>) this.repository.findAll();
	}
	
	public Optional<Country> findById(Integer id) {

		return this.repository.findById(id);
	}

	public Country save(Country country) {

		return this.repository.save(country);

	}

	public Country update(Country country) {

		return this.repository.save(country);

	}
	
	public void deleteById(Integer id) {

		 this.repository.deleteById(id);

	}
}
