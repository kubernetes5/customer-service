package com.ansp.customer.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.customer.entities.*;
import com.ansp.customer.repository.CustomerRepository;



@Service
public class CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	public List<Customer> findAll(){
		return (List<Customer>) this.repository.findAll();
	}
	
	public Optional<Customer> findById(Integer id) {

		return this.repository.findById(id);
	}

	public Customer save(Customer customer) {

		return this.repository.save(customer);

	}

	public Customer update(Customer customer) {

		return this.repository.save(customer);

	}
	
	public void deleteById(Integer id) {

		 this.repository.deleteById(id);

	}
}
