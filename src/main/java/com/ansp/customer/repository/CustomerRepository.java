package com.ansp.customer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ansp.customer.entities.*;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer>{

}
