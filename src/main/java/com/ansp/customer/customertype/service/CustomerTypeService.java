package com.ansp.customer.customertype.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ansp.customer.customertype.repository.CustomerTypeRepository;
import com.ansp.customer.entities.CustomerType;

@Service
public class CustomerTypeService {
	
	@Autowired
	private CustomerTypeRepository repository;
	
	public List<CustomerType> findAll(){
		return (List<CustomerType>) this.repository.findAll();
	}
	
	public Optional<CustomerType> findById(Integer id) {

		return this.repository.findById(id);
	}

	public CustomerType save(CustomerType customertype) {

		return this.repository.save(customertype);

	}

	public CustomerType update(CustomerType customertype) {

		return this.repository.save(customertype);

	}
	
	public void deleteById(Integer id) {

		 this.repository.deleteById(id);

	}

}
