package com.ansp.customer.customertype.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.customer.customertype.service.CustomerTypeService;
import com.ansp.customer.entities.CustomerType;

@RestController
public class CustomerTypeController {
	
	@Autowired
	private CustomerTypeService service;
	
	@GetMapping("/customertypes")
	public List<CustomerType> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("/customertypes")
	public CustomerType save(@RequestBody CustomerType customertype) {
		return this.service.save(customertype);
	}
	
	@PutMapping("/customertypes/{id}")
	public CustomerType update(@RequestBody CustomerType customertype, @PathVariable("id") Integer id) {
		customertype.setId(id);
		return this.service.update(customertype);
	}
	
	@GetMapping("/customertypes/{id}")
	public Optional<CustomerType> update(@PathVariable("id") Integer id) {
		return this.service.findById(id);
	}
	
	@DeleteMapping("/customertypes/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
