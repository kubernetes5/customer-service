package com.ansp.customer.customertype.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ansp.customer.entities.*;

@Repository
public interface CustomerTypeRepository extends CrudRepository<CustomerType, Integer>{

}
