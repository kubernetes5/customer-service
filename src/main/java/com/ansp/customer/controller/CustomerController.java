package com.ansp.customer.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ansp.customer.entities.Customer;
import com.ansp.customer.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	@GetMapping("/customers")
	public List<Customer> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("/customers")
	public Customer save(@RequestBody Customer customer) {
		return this.service.save(customer);
	}
	
	@PutMapping("/customers/{id}")
	public Customer update(@RequestBody Customer customer, @PathVariable("id") Integer id) {
		customer.setId(id);
		return this.service.update(customer);
	}
	
	@GetMapping("/customers/{id}")
	public Optional<Customer> update(@PathVariable("id") Integer id) {
		return this.service.findById(id);
	}
	
	@DeleteMapping("/customers/{id}")
	public void delete(@PathVariable("id") Integer id) {
		this.service.deleteById(id);
	}
}
